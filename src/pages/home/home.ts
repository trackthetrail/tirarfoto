import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { getLocaleTimeFormat } from '@angular/common';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  base64Image: string;
  
  constructor(public navCtrl: NavController, private _camera: Camera) {
  
    this.takePicture();
  }
  
  takePicture(){
    const options: CameraOptions = {
      quality: 100,
      destinationType: this._camera.DestinationType.FILE_URI,
      encodingType: this._camera.EncodingType.JPEG,
      mediaType: this._camera.MediaType.PICTURE,
      saveToPhotoAlbum: true
    }

    this._camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      this.base64Image = imageData;
     }, (err) => {
      // Handle error
     });
    }  
}


 
